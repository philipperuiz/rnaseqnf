Scripts to analys RNASeq datas with nfcore pipeline and R.
Script is writing for the genotoul cluster

It is based one the [nextflow tutorial of genotoul platform](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/pages/nextflow_usage/)

1 - Preparing directory :

```bash
mkdir Project
mkdir Project/data
mkdir Project/ref
```

2 - Preparing data :
- copy datas in data (fq.gz) and ref (fasta, gtf) folder 
- create `sample.csv` file :

```bash
sample,fastq_1,fastq_2,strandedness
CONTROL_REP1,data/sample1_R1.fastq.gz,data/sample1_R2.fastq.gz,auto
CONTROL_REP1,data/sample2_R1.fastq.gz,data/sample2_R2.fastq.gz,auto
CONTROL_REP1,data/sample3_R1.fastq.gz,data/sample3_R2.fastq.gz,auto
(...)
```

3 - adapte slurm script


