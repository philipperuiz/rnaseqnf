#!/bin/bash
#SBATCH -J nfcore-rnaseq
#SBATCH -p unlimitq
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --mail-user=philippe.ruiz@inrae.fr
#SBATCH --mail-type=ALL

# modules load
module load bioinfo/Nextflow/24.04.2 # latest
module load devel/java/17.0.6 # latest
module load containers/singularity/3.9.9

# launch rnaseg nextflow pipeline

nextflow run nf-core/rnaseq -profile genotoul \
--input sample.csv \
--fasta ref/ref.fasta \
--outdir output \
--gtf ref/ref.gtf \
--aligner hisat2 \
-r 3.14.0
